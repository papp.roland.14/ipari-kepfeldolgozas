import os
os.environ['TF_ENABLE_ONEDNN_OPTS'] = '0'
import tensorflow as tf
# path join miatt kellhet
import cv2
# python 3.13-tól nem lesz
import imghdr
from matplotlib import pyplot as plt
import numpy as np
import glob

# SEEMS TO BE AN ERROR BUT IT RUNS
from tensorflow.keras.models import Sequential
# Layer-eket építünk különböző modulokból, minegyiknek elérhető a leírása!
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Dense, Flatten
from tensorflow.keras.metrics import Precision, Recall, BinaryAccuracy

def images_getter(folder, extension):
    images = glob.glob(folder + extension)
    # Fájlok rendezése a sort_key kulcsfüggvény segítségével
    return images

def write_predictions_to_file(kepek, predictions, file_name):
    # Ellenőrizzük, hogy a kepek és predictions listák egyforma hosszúságúak-e
    if len(kepek) != len(predictions):
        raise ValueError("A 'kepek' és 'predictions' listák hosszúságának meg kell egyeznie.")

    # Megnyitjuk a fájlt írásra
    with open(file_name, 'w') as file:
        # Feldolgozzuk az elemeket
        for kep, prediction in zip(kepek, predictions):
            # Levágjuk az első 7 karaktert a kép nevéből
            truncated_kep = kep[5:]
            # Kiírjuk a fájlba az eredményt
            file.write(f"{truncated_kep}\t{prediction}\n")

# ------------------ DATA LOADING ---------------------

# Nem adjuk oda a tensorflownak az összes VRAM-ot
gpus = tf.config.experimental.list_physical_devices('GPU')
for gpu in gpus:
    tf.config.experimental.set_memory_growth(gpu, True)

# Ki lehet szűrni a nem megfelelő képfájlokat - ez most nem kell mert megbízható adatbázisból származnak
data_dir = 'data'
image_exts = ['jpeg', 'jpg', 'bmp', 'png']

"""
for image_class in os.listdir(data_dir):
    for image in os.listdir(os.path.join(data_dir, image_class)):
        image_path = os.path.join(data_dir, image_class, image)
        try:
            img = cv2.imread(image_path)
            tip = imghdr.what(image_path)
            if tip not in image_exts:
                print('Image not in ext list {}'.format(image_path))
                os.remove(image_path)
        except Exception as e:
            print('Issue with image {}'.format(image_path))
            # os.remove(image_path)
"""
data = tf.keras.utils.image_dataset_from_directory('data')

"""

data_iterator = data.as_numpy_iterator()
# Get another batch from the iterator
# batch = a quantity or number coming at one time or taken together
batch = data_iterator.next()

# ki is rajzoltathatjuk

fig, ax = plt.subplots(ncols=4, figsize=(20,20))
for idx, img in enumerate(batch[0][:4]):
    ax[idx].imshow(img.astype(int))
    ax[idx].title.set_text(batch[1][idx])

"""
# ------------------ PREPROCESSING ---------------------

# normalisation from RGB 0-255 to 0-1
# x: images, y: labels
# map végig megy a dataset-en
data = data.map(lambda x, y: (x/255, y))
scaled_iterator = data.as_numpy_iterator()
batch = scaled_iterator.next()

# ------------------ SPLIT DATA ---------------------

train_size = int(len(data)*.7)
val_size = int(len(data)*.2)+1
test_size = int(len(data)*.1)+1

# take-skip, a skip átugorja a BATCH-ban azt a részt, amit a train-ben már take-el felhasnzáltunk
train = data.take(train_size)
val = data.skip(train_size).take(val_size)
test = data.skip(train_size+val_size).take(test_size)

# ------------------ MODEL BUILDING ---------------------
# létrehozunk egy szekvenciális modellt, amibe behívjük a különböző eljárásainkat
model = Sequential()
# eljárások
# konvolúció: 16 db 3x3 szűrő, 1 pixeles lépegetéssel
# relu: minden negatív értéket 0-ba visz, a pozitívokat megtartja
model.add(Conv2D(16, (3,3), 1, activation='relu', input_shape=(256,256,3)))
# vmiféle tömörítési eljárás
model.add(MaxPooling2D())
# megint egy konvolúciós blokk
model.add(Conv2D(32, (3,3), 1, activation='relu'))
model.add(MaxPooling2D())
# megint egy konvolúciós blokk
model.add(Conv2D(16, (3,3), 1, activation='relu'))
model.add(MaxPooling2D())
# ~egyetlen értékbe sűrítjük az eredményt, dimenziótlanítjuk
model.add(Flatten())
model.add(Dense(256, activation='relu'))
# sigmoid activation hozza meg a döntést, 0 és 1, a 2 osztály esetén
model.add(Dense(1, activation='sigmoid'))
# 'adam' --> optimalizáló
model.compile('adam', loss=tf.losses.BinaryCrossentropy(), metrics=['accuracy'])
#model.summary()

# ------------------ TRAIN ---------------------
# logok
logdir='logs'
tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=logdir)

# modellre illesztés, 1 epoch 1 teljes futás a modellen
hist = model.fit(train, epochs=20, validation_data=val, callbacks=[tensorboard_callback])

# hist => history tartalmazza a futási eredményeket

"""
fig = plt.figure()
plt.plot(hist.history['loss'], color='teal', label='loss')
plt.plot(hist.history['val_loss'], color='orange', label='val_loss')
fig.suptitle('Loss', fontsize=20)
plt.legend(loc="upper left")
plt.show()

fig = plt.figure()
plt.plot(hist.history['accuracy'], color='teal', label='accuracy')
plt.plot(hist.history['val_accuracy'], color='orange', label='val_accuracy')
fig.suptitle('Accuracy', fontsize=20)
plt.legend(loc="upper left")
plt.show()

"""
# ------------------ EVALUATING ---------------------

pre = Precision()
re = Recall()
acc = BinaryAccuracy()

for batch in test.as_numpy_iterator():
    # X --> set of images, y --> true value
    X, y = batch
    yhat = model.predict(X)
    pre.update_state(y, yhat)
    re.update_state(y, yhat)
    acc.update_state(y, yhat)

#print(pre.result(), re.result(), acc.result())

# ------------------ TESTING ---------------------

img = cv2.imread('test/traffic_lights.jpg')
resize = tf.image.resize(img, (256,256))
yhat = model.predict(np.expand_dims(resize/255, 0))

if yhat > 0.5:
    print(f'Predicted class is Lamp')
else:
    print(f'Predicted class is Pedestrian')


kepek=images_getter('test','/*')
predictions = []
for i, img in enumerate(kepek):

    img_read = cv2.imread(img)
    #plt.imshow(img)
    #plt.show()
    resize = tf.image.resize(img_read, (256,256))

    # modell 1 listát vár, ezmiatt át kell alakítani
    np.expand_dims(resize, 0)
    yhat = model.predict(np.expand_dims(resize/255, 0))


    if yhat > 0.5:
        predictions.append('Lamp')
    else:
        predictions.append('Pedestrian Crossing')

write_predictions_to_file(kepek, predictions, "output.txt")