import cv2
import glob
import numpy as np

def categorize_images(images, reference_images):
    # Kezdetben minden képet a "beazonosítatlan" kategóriába sorolunk
    result = ["beazonosítatlan" for _ in range(len(images))]

    # ORB descriptor létrehozása
    sift = cv2.SIFT_create()

    # FLANN alapű párosítás konfiguráció
    FLANN_INDEX_KDTREE = 1
    index_params = dict(algorithm=FLANN_INDEX_KDTREE, trees=5)
    search_params = dict(checks=50)  # or pass empty dictionary

    flann = cv2.FlannBasedMatcher(index_params, search_params)
    MIN_MATCH_COUNT=10;

    # Képek feldolgozása
    for i, img in enumerate(images):
        # A kép feldolgozása
        img_read = cv2.imread(img)
        gray_img = cv2.cvtColor(img_read, cv2.COLOR_BGR2GRAY)
        # Keypoints és descriptors
        kp, des = sift.detectAndCompute(gray_img, None)

        # Az aktuális legjobb illeszkedés tárolása
        best_match_tag = None
        best_match_count = 0

        # Képek összehasonlítása a referencia képekkel
        for j, ref_img in enumerate(reference_images):
            img_ref_read = cv2.imread(ref_img)
            ref_gray_img = cv2.cvtColor(img_ref_read, cv2.COLOR_BGR2GRAY)
            ref_kp, ref_des = sift.detectAndCompute(ref_gray_img, None)

            # Párosítások keresése
            matches = flann.knnMatch(des, ref_des, k=2)
            good_matches = []
            try:
                for m, n in matches:
                    if m.distance < 0.7 * n.distance:
                        good_matches.append(m)
            except ValueError:
                continue  # Ha a matches üres, akkor folytatjuk a következő iterációt

            if len(good_matches) > MIN_MATCH_COUNT:
                # Homográfia
                src_pts = []
                dst_pts = []

                for m in good_matches:
                    src_pts.append(kp[m.queryIdx].pt)
                    dst_pts.append(ref_kp[m.trainIdx].pt)
                src_pts, dst_pts = np.float32((src_pts, dst_pts))

                H, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 5.0)
                matchesMask = mask.ravel().tolist()

                # Itt ki lehetne rajzoltatni, de más a megközelítés most!
                # És egyébként le sem fut: cv2.perspectiveTransform-ban van valami baja, valószínűleg bekavar a for
                """
                
                h1, w1 = img_read.shape[:2]
                h2, w2 = img_ref_read.shape[:2]

                bounding_points = np.float32([ [0,0],[0,h1-1],[w1-1,h1-1],[w1-1,0] ]).reshape(-1,1,2)

                transformed_points = cv2.perspectiveTransform(bounding_points, H)
                int_transformed_points = np.int32(transformed_points)
                img_ref_read = cv2.polylines(img_ref_read, [int_transformed_points], True, 255, 10, cv2.LINE_AA)


                draw_params = dict(matchColor = (0,255,0),
                   singlePointColor = None,
                   matchesMask = matchesMask,
                   flags = 2)

                found_template_img = cv2.drawMatches(img1,keypoints1,img2,keypoints2,good,None,**draw_params)
                cv2.imwrite("template_found.png", found_template_img)
                
                """

                # Ha van legalább 10 jó párosítás
                match_count = len(good_matches)
                if match_count > best_match_count:
                    best_match_tag = ref_img
                    best_match_count = match_count
        if best_match_tag is not None:
            result[i] = best_match_tag[9:-6]
    return result

def sort_key(filename):
    # A fájlnév szétválasztása a számrészre és a stringrészre
    number_part = int(''.join(filter(str.isdigit, filename)))
    return number_part

def images_getter(folder, extension):
    images = glob.glob(folder + extension)
    # Fájlok rendezése a sort_key kulcsfüggvény segítségével
    return sorted(images, key=sort_key)


kepek=images_getter('images','/*.png')
ref_kepek=images_getter('base_pic','/*.png')

categorised=categorize_images(kepek,ref_kepek)

with open("output.txt", "w") as file:
    # Két oszlopban megjelenítés
    for i, (img_path, category) in enumerate(zip(kepek, categorised)):
        img_name = img_path[7:]  # Az első 7 karakter levágása
        file.write(f"{i+1}. Kép: {img_name} | Kategória: {category}\n")
